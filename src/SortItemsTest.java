import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SortItemsTest {
	
	SortItems sortItems = new SortItems();
	

	@Test
	void testSortItemsOfArrays() {
		Item iphone = new Item("iPhone", 1, 45);
		Item xiaomi = new Item("Xiaomi", 2, 60);
		Item samsung = new Item("Samsung", 3, 40);
		
		Item[] allItems = {samsung,iphone,xiaomi};
		
		
		Item[] testItems = sortItems.sortItemsOfArrays();
		
		
		assertNotSame(allItems,testItems);
	}

}
