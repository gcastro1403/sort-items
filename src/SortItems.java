import java.util.Arrays;

public class SortItems {

	public Item[] sortItemsOfArrays () {
		
	Item iphone = new Item("iPhone", 1, 45);
	Item xiaomi = new Item("Xiaomi", 2, 60);
	Item samsung = new Item("Samsung", 3, 40);
	
	Item[] allItems = {iphone,xiaomi,samsung};
	
	Arrays.sort(allItems,(a ,b) -> a.getPrice() - b.getPrice());
	System.out.println(Arrays.toString(allItems));	
	return allItems;
		
	}
}
