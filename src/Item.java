
public class Item {
	
	private String nombre;
	
	private Integer id;
	
	private Integer price;
	
	
	Item(String nombre, Integer id,Integer price  ){
		
		this.nombre = nombre;
		this.id = id;
		this.price = price;
		
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}
	
	@Override
    public String toString() {
        return "{"+ this.nombre +":" + this.price+"}" ;
    }

}
